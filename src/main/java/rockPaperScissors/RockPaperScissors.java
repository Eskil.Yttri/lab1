package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {

    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }


    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public void run() {
        // TODO: Implement Rock Paper Scissors
        System.out.println("Let's play round " + roundCounter);

        while (true) {
            String[] rps = {"rock", "paper", "scissors"};
            String computerMove = rps[new Random().nextInt(rps.length)];

            String playerMove;

            while (true) {
                System.out.println("Your choice (Rock/Paper/Scissors)?");
                playerMove = sc.nextLine();
                if (playerMove.equals("rock") || playerMove.equals("paper") || playerMove.equals("scissors")) {
                    break;
                }
                System.out.println(playerMove + " is not a valid move.");
            }

            //System.out.println("Human chose "+ playerMove", computer chose "computerMove". "is_winner" wins! ");

            if (playerMove.equals(computerMove)) {
                System.out.println("Human chose "+ playerMove +", computer chose " + computerMove +". it's a tie!");
            } else if (playerMove.equals("rock")) {
                if (computerMove.equals("paper")) {
                    computerScore++;
                    System.out.println("Human chose "+ playerMove +", computer chose " + computerMove +". Computer wins!");
                }
                // System.out.println("You lose!");

            } else if (computerMove.equals("scissors")) {
                humanScore++;
                System.out.println("Human chose "+ playerMove +", computer chose " + computerMove +". Human wins!");
            } else if (playerMove.equals("paper")) {
                if (computerMove.equals("rock")) {
                    System.out.println("Human chose "+ playerMove +", computer chose " + computerMove +". Human wins!");

                } else if (computerMove.equals("scissors")) {
                    System.out.println("Human chose "+ playerMove +", computer chose " + computerMove +". Computer wins!");
                }
            } else if (playerMove.equals("scissors")) {
                if (computerMove.equals("paper")) {
                    humanScore++;
                }
                System.out.println("Human chose "+ playerMove +", computer chose " + computerMove +". Human wins!");

            } else if (computerMove.equals("rock")) {
                System.out.println("Human chose "+ playerMove +", computer chose " + computerMove +". Computer wins!");
            }
        }
        System.out.println("Score: human " + humanScore + " , computer " + computerScore);
        roundCounter++;

        System.out.println("Do you wish to continue playing? (y/n)?");
        String playAgain = sc.nextLine();
        if (!playAgain.equals("y")) {
            System.out.println("Bye Bye :)");
            break;
        }
        sc.close();
        }

    /**
     * Reads input from console with given prompt
     *
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
}

